# nix-enter-build

An executable to jump into exactly the environment that Nix will build
your derivation in. Drops you into a nix-shell on your derivation, inside
a /tmp directory with the contents that Nix will build.

Call it like so:

```bash
nix-enter-build default.nix
```

The first (and only) argument must be a filename of a no-argument expression
that produces a derivation.

A limitation is that it must be an expression file, not the actual derivation
in the Nix store, and it cannot be an absolute path.

Additionally, the derivation must be produced using stdenv.mkDerivation.
