{ pkgs ? import <nixpkgs> {} }:

import ./nix-enter-build.nix {
  inherit (pkgs) nix ripgrep;
  mkDerivation = pkgs.stdenv.mkDerivation;
}
