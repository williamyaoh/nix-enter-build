#!/usr/bin/bash

# TODO: This doesn't have a --help option or any kind of command line help.
# Assuming that you already know how to use it, that's fine, but y'know...

DRV=$1
TMPDIR=$(@nix@/bin/nix-build --keep-failed -E "(import ./$DRV).overrideAttrs (oldAttrs: { \
  dontConfigure = false; \
  configurePhase = '' \
    exit 1 \
  ''; \
})" \
  2>&1 \
  | @ripgrep@/bin/rg "^note: keeping build directory '([^']*)'$" -r '$1'
)

if [ -d $TMPDIR ]; then
  @nix@/bin/nix-shell --pure $DRV --command "cd $TMPDIR; return"
else
  >&2 echo "ERROR: couldn't find Nix's build tmpdir."
  >&2 echo 'ERROR: something has gone horribly wrong. try building with'
  >&2 echo 'ERROR: `nix-build --keep-failed` manually.'
  exit 1
fi
